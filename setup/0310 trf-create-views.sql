



--------------------------------------------------------------------------------
-- Project authorization: views with columns "pro_id" "<pk>"
--------------------------------------------------------------------------------

-- organisation (special case for project-authorization)
drop view if exists trf.v_auth_organisation;
create view trf.v_auth_organisation as
	select
		pro.pro_id,
		pro.pro_org_id as org_id
	from trf.project as pro
;
-- block
drop view if exists trf.v_auth_block;
create view trf.v_auth_block as
	select
		blk.blk_pro_id as pro_id,
		blk.blk_id
	from trf.block as blk
;
