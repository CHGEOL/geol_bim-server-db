-- ############################################################
-- Adding auditing metadata for codesystem
-- ############################################################

-- -----------------------------------------------------------------------------
-- add auditing metadata and functions
-- -----------------------------------------------------------------------------
alter table trf.codelist 
	add column col_create_date timestamptz not null default current_timestamp, 
	add column col_create_user varchar(12) not null default left(current_user,12),
	add column col_change_date timestamptz not null default current_timestamp,
	add column col_change_user varchar(12) not null default left(current_user,12)
;

alter table trf.code
	add column cod_create_date timestamptz not null default current_timestamp, 
	add column cod_create_user varchar(12) not null default left(current_user,12),
	add column cod_change_date timestamptz not null default current_timestamp,
	add column cod_change_user varchar(12) not null default left(current_user,12)
;

