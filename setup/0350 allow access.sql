

grant usage on schema trf 			to apisrv_data_admin;
grant usage on schema usr 			to apisrv_data_admin;

grant all on all tables in schema trf  			to apisrv_data_admin;
grant all on all tables in schema usr 			to apisrv_data_admin;

-- Acces for apisrv_user
grant usage on schema trf to apisrv_user;
grant select on all tables in schema trf to apisrv_user;




