
create role apisrv_dba 				login 	inherit password 'apisrv_dba';
create role apisrv_data_admin 			nologin inherit password 'apisrv_data_admin';
create role apisrv_user 				nologin inherit password 'apisrv_user';
create role apisrv_imp 				login 	inherit password 'apisrv_imp';
create role apisrv_js                 login   inherit password 'apisrv_js';

grant apisrv_data_admin to apisrv_dba with admin option;
grant apisrv_user 		 to apisrv_dba with admin option;
grant apisrv_data_admin to apisrv_imp;
