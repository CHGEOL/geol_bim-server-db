-- -------------------------------------------------------------------------------------------------
-- codelist
-- -------------------------------------------------------------------------------------------------
INSERT INTO trf.codelist (col_id, col_name) VALUES('lg', 'language');
INSERT INTO trf.codelist (col_id, col_name) VALUES('ros', 'role_system');
INSERT INTO trf.codelist (col_id, col_name) VALUES('pro', 'project_type');
--INSERT INTO trf.codelist (col_id, col_name) VALUES('dat', 'datatype');
INSERT INTO trf.codelist (col_id, col_name) VALUES('tok', 'token_type');
INSERT INTO trf.codelist (col_id, col_name) VALUES('bkt', 'block_type');
INSERT INTO trf.codelist (col_id, col_name) VALUES('bks', 'block_state');

-- -------------------------------------------------------------------------------------------------
-- codes
-- -------------------------------------------------------------------------------------------------

INSERT INTO trf.code (cod_id, cod_col_id, cod_code, cod_name, cod_seq) VALUES('lg-dech' , 'lg', 'de-ch', 'Deutsch - Schweiz', 0);
--INSERT INTO trf.code (cod_id, cod_col_id, cod_code, cod_name, cod_seq) VALUES('lg-frch' , 'lg', 'fr-ch', 'Francais - Suisse', 0);
--INSERT INTO trf.code (cod_id, cod_col_id, cod_code, cod_name, cod_seq) VALUES('lg-enus', 'lg', 'en-us', 'English - US', 0);


INSERT INTO trf.code (cod_id, cod_col_id, cod_code, cod_name, cod_seq) VALUES('ros-acon', 'ros', 'app-conn' , 'Applikation-Connect (Login)', 0);
INSERT INTO trf.code (cod_id, cod_col_id, cod_code, cod_name, cod_seq) VALUES('ros-aadm', 'ros', 'app-admin', 'Applikation-Administrator', 0);
INSERT INTO trf.code (cod_id, cod_col_id, cod_code, cod_name, cod_seq) VALUES('ros-oadm', 'ros', 'org-admin', 'Organisation-Administrator', 0);
INSERT INTO trf.code (cod_id, cod_col_id, cod_code, cod_name, cod_seq) VALUES('ros-opl',  'ros', 'org-pl'   , 'Organisation-Projektleiter', 0);
INSERT INTO trf.code (cod_id, cod_col_id, cod_code, cod_name, cod_seq) VALUES('ros-oemp', 'ros', 'org-emp'  , 'Organisation-Mitarbeitender', 0);
INSERT INTO trf.code (cod_id, cod_col_id, cod_code, cod_name, cod_seq) VALUES('ros-oprs', 'ros', 'org-prs', 'Persönliche Organisation', 0);
INSERT INTO trf.code (cod_id, cod_col_id, cod_code, cod_name, cod_seq) VALUES('ros-padm', 'ros', 'pro-admin', 'Projekt-Administrator', 0);
INSERT INTO trf.code (cod_id, cod_col_id, cod_code, cod_name, cod_seq) VALUES('ros-prd',  'ros', 'pro-read' , 'Projekt-Read', 0);
INSERT INTO trf.code (cod_id, cod_col_id, cod_code, cod_name, cod_seq) VALUES('ros-pwr' , 'ros', 'pro-write', 'Projekt-Write', 0);

INSERT INTO trf.code (cod_id, cod_col_id, cod_code, cod_name, cod_seq) VALUES('pro-ref' , 'pro', 'ref'  , 'Referenzprojekt', 0);
INSERT INTO trf.code (cod_id, cod_col_id, cod_code, cod_name, cod_seq) VALUES('pro-usr' , 'pro', 'user' , 'Benutzerprojekt', 0);

-- INSERT INTO trf.code (cod_id, cod_col_id, cod_code, cod_name, cod_seq) VALUES('dat-str' , 'dat', 'str' , 'String', 0);
-- INSERT INTO trf.code (cod_id, cod_col_id, cod_code, cod_name, cod_seq) VALUES('dat-nbr' , 'dat', 'nbr' , 'Number', 0);
-- INSERT INTO trf.code (cod_id, cod_col_id, cod_code, cod_name, cod_seq) VALUES('dat-bln' , 'dat', 'bln' , 'Boolean', 0);
-- INSERT INTO trf.code (cod_id, cod_col_id, cod_code, cod_name, cod_seq) VALUES('dat-dat' , 'dat', 'dat' , 'Date', 0);
-- INSERT INTO trf.code (cod_id, cod_col_id, cod_code, cod_name, cod_seq) VALUES('dat-dom' , 'dat', 'dom' , 'Domain', 0);

INSERT INTO trf.code (cod_id, cod_col_id, cod_code, cod_name, cod_seq) VALUES('tok-crg' , 'tok', 'crg'  , 'Confirm Registration', 0);
INSERT INTO trf.code (cod_id, cod_col_id, cod_code, cod_name, cod_seq) VALUES('tok-rpw' , 'tok', 'rpw'  , 'Reset Password', 0);

-- INSERT INTO trf.code (cod_id, cod_col_id, cod_code, cod_name, cod_seq) VALUES('bkt-cfg' , 'bkt', 'type-cfg'  , 'Config data', 10);
INSERT INTO trf.code (cod_id, cod_col_id, cod_code, cod_name, cod_seq) VALUES('bkt-pcf' , 'bkt', 'type-pcf'  , 'Point config data', 110);
INSERT INTO trf.code (cod_id, cod_col_id, cod_code, cod_name, cod_seq) VALUES('bkt-pnt' , 'bkt', 'type-pnt'  , 'Point data', 115);
INSERT INTO trf.code (cod_id, cod_col_id, cod_code, cod_name, cod_seq) VALUES('bkt-lin' , 'bkt', 'type-lin'  , 'Line data', 120);
INSERT INTO trf.code (cod_id, cod_col_id, cod_code, cod_name, cod_seq) VALUES('bkt-acf' , 'bkt', 'type-acf'  , 'Area config data', 130);
INSERT INTO trf.code (cod_id, cod_col_id, cod_code, cod_name, cod_seq) VALUES('bkt-ara' , 'bkt', 'type-ara'  , 'Area data', 135);
INSERT INTO trf.code (cod_id, cod_col_id, cod_code, cod_name, cod_seq) VALUES('bkt-hdr' , 'bkt', 'type-hdr'  , 'Header text', 210);
INSERT INTO trf.code (cod_id, cod_col_id, cod_code, cod_name, cod_seq) VALUES('bkt-div' , 'bkt', 'type-div'  , 'Division text', 220);

INSERT INTO trf.code (cod_id, cod_col_id, cod_code, cod_name, cod_seq) VALUES('bks-ini' , 'bks', 'state-ini'  , 'Initial state', 0);
INSERT INTO trf.code (cod_id, cod_col_id, cod_code, cod_name, cod_seq) VALUES('bks-upl' , 'bks', 'state-upl'  , 'Uploading state', 0);
INSERT INTO trf.code (cod_id, cod_col_id, cod_code, cod_name, cod_seq) VALUES('bks-trf' , 'bks', 'state-trf'  , 'Transforming state', 0);
INSERT INTO trf.code (cod_id, cod_col_id, cod_code, cod_name, cod_seq) VALUES('bks-rdy' , 'bks', 'state-rdy'  , 'Data ready state', 0);
INSERT INTO trf.code (cod_id, cod_col_id, cod_code, cod_name, cod_seq) VALUES('bks-upf' , 'bks', 'state-upf'  , 'Uploading failed state', 0);
INSERT INTO trf.code (cod_id, cod_col_id, cod_code, cod_name, cod_seq) VALUES('bks-tff' , 'bks', 'state-tff'  , 'Transforming failed state', 0);

-- -------------------------------------------------------------------------------------------------
-- check_constraints for all tables that are using codes
-- -------------------------------------------------------------------------------------------------
create or replace function trf.code_check (p_cod_id text, p_col_id text) returns boolean 
as 
-- checks wheter a p_cod_id is within a codelist (p_col_id)
$$
select 
( p_cod_id in
	(select cod_id from trf.code where cod_col_id = p_col_id)
);

$$ language sql;



-- lg


-- ros
alter table usr.user_application
	add constraint check_uap_role_code 
	check (trf.code_check(uap_role_code,'ros'))
	;
alter table usr.user_organisation
	add constraint check_uog_role_code 
	check (trf.code_check(uog_role_code,'ros'))
	;
alter table usr.user_project
	add constraint check_upr_role_code 
	check (trf.code_check(upr_role_code,'ros'))
	;

-- pro
alter table trf.project
	add constraint check_pro_type_code 
	check (trf.code_check(pro_type_code,'pro'))
	;

-- tok
alter table usr.token
	add constraint check_tok_type_code
	check (trf.code_check(tok_token_code,'tok'))
	;
