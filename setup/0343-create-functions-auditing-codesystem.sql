-- ############################################################
-- Adding/updating auditing-triggers/functions to tables for codesystem
-- ############################################################

-- -----------------------------------------------------------------------------
-- CODELIST auditing functions/triggers
-- -----------------------------------------------------------------------------
create or replace function trf.codelist_audit_trg() returns trigger as
$$
begin
	if tg_op = 'INSERT' then
		return new;
	elsif tg_op = 'UPDATE' then
		new.col_change_date := now();
		return new;
	elsif tg_op = 'DELETE' then
		return old;
	end if;

end;
$$ language plpgsql;

drop trigger if exists col_audit on trf.codelist;

create trigger col_audit
BEFORE INSERT OR UPDATE OR delete on trf.codelist
for each row
	execute procedure trf.codelist_audit_trg();

-- -----------------------------------------------------------------------------
-- CODE auditing functions/triggers
-- -----------------------------------------------------------------------------
create or replace function trf.code_audit_trg() returns trigger as
$$
begin
	if tg_op = 'INSERT' then
		return new;
	elsif tg_op = 'UPDATE' then
		new.cod_change_date := now();
		return new;
	elsif tg_op = 'DELETE' then
		return old;
	end if;

end;
$$ language plpgsql;

drop trigger if exists cod_audit on trf.code;

create trigger cod_audit
BEFORE INSERT OR UPDATE OR delete on trf.code
for each row
	execute procedure trf.code_audit_trg();

