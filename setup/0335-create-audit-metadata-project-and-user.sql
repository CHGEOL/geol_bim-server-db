-- #################################################################
-- Adding auditing-metadaten to tables for project/organisation/user
-- #################################################################

alter table trf.project
	add column pro_create_date timestamptz not null default current_timestamp,
	add column pro_create_user varchar(12) not null default left(current_user,12),
	add column pro_change_date timestamptz not null default current_timestamp,
	add column pro_change_user varchar(12) not null default left(current_user,12)
;

alter table trf.block
	add column blk_create_date timestamptz not null default current_timestamp,
	add column blk_create_user varchar(12) not null default left(current_user,12),
	add column blk_change_date timestamptz not null default current_timestamp,
	add column blk_change_user varchar(12) not null default left(current_user,12)
;

alter table usr.organisation
	add column org_create_date timestamptz not null default current_timestamp,
	add column org_create_user varchar(12) not null default left(current_user,12),
	add column org_change_date timestamptz not null default current_timestamp,
	add column org_change_user varchar(12) not null default left(current_user,12)
;

alter table usr.user
	add column usr_create_date timestamptz not null default current_timestamp,
	add column usr_create_user varchar(12) not null default left(current_user,12),
	add column usr_change_date timestamptz not null default current_timestamp,
	add column usr_change_user varchar(12) not null default left(current_user,12)
;

alter table usr.user_application
	add column uap_create_date timestamptz not null default current_timestamp,
	add column uap_create_user varchar(12) not null default left(current_user,12),
	add column uap_change_date timestamptz not null default current_timestamp,
	add column uap_change_user varchar(12) not null default left(current_user,12)
;

alter table usr.user_organisation
	add column uog_create_date timestamptz not null default current_timestamp,
	add column uog_create_user varchar(12) not null default left(current_user,12),
	add column uog_change_date timestamptz not null default current_timestamp,
	add column uog_change_user varchar(12) not null default left(current_user,12)
;

alter table usr.user_project
	add column upr_create_date timestamptz not null default current_timestamp,
	add column upr_create_user varchar(12) not null default left(current_user,12),
	add column upr_change_date timestamptz not null default current_timestamp,
	add column upr_change_user varchar(12) not null default left(current_user,12)
;

