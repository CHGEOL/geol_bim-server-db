--------------------------------------------------------------------------------
-- User and all the assigned roles: user.*, "id", "role_code"
--------------------------------------------------------------------------------

-- "id": PK of the related object, e.g. project-id for project roles (padm, pwr, prd)

DROP VIEW IF EXISTS usr.v_user_roles;

CREATE VIEW usr.v_user_roles as
	-- user_application
	SELECT
		usr.*,
		usr.usr_id AS id, uap.uap_role_code AS role_code,
		NULL AS usr_org_name
	FROM
 		usr."user" AS usr
		LEFT JOIN usr.user_application AS uap ON uap.uap_usr_id = usr.usr_id
	UNION ALL
	-- user_organisation (org_name, if employee or personal organisation)
	SELECT
		usr.*,
		uog.uog_org_id AS id, uog.uog_role_code AS role_code,
		org.org_name AS usr_org_name
	FROM
		usr."user" AS usr
		LEFT JOIN usr.user_organisation AS uog ON uog.uog_usr_id = usr.usr_id
		LEFT JOIN usr.organisation AS org
		ON org.org_id = uog.uog_org_id AND (uog.uog_role_code = 'ros-oemp' OR uog.uog_role_code = 'ros-oprs')
	UNION ALL
	-- user_project
	SELECT
		usr.*,
		upr.upr_pro_id AS id, upr.upr_role_code AS role_code,
		NULL AS usr_org_name
	FROM
		usr."user" AS usr
		LEFT JOIN usr.user_project AS upr ON upr.upr_usr_id = usr.usr_id
;
