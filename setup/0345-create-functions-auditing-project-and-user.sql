-- ####################################################################################
-- Adding/updating auditing-triggers/functions to tables for project/organisation/user
-- ####################################################################################

-- -----------------------------------------------------------------------------
-- PROJECT auditing functions/triggers
-- -----------------------------------------------------------------------------
create or replace function trf.project_audit_trg() returns trigger as
$$
begin
	if tg_op = 'INSERT' then
		return new;
	elsif tg_op = 'UPDATE' then
		new.pro_change_date := now();
		return new;
	elsif tg_op = 'DELETE' then
		return old;
	end if;

end;
$$ language plpgsql;

drop trigger if exists pro_audit on trf.project;

create trigger pro_audit
BEFORE INSERT OR UPDATE OR delete on trf.project
for each row
	execute procedure trf.project_audit_trg();


-- -----------------------------------------------------------------------------
-- BLOCK auditing functions/triggers
-- -----------------------------------------------------------------------------
create or replace function trf.block_audit_trg() returns trigger as
$$
begin
	if tg_op = 'INSERT' then
		return new;
	elsif tg_op = 'UPDATE' then
		new.blk_change_date := now();
		return new;
	elsif tg_op = 'DELETE' then
		return old;
	end if;

end;
$$ language plpgsql;

drop trigger if exists blk_audit on trf.block;

create trigger blk_audit
BEFORE INSERT OR UPDATE OR delete on trf.block
for each row
	execute procedure trf.block_audit_trg();


-- -----------------------------------------------------------------------------
-- ORGANISATION auditing functions/triggers
-- -----------------------------------------------------------------------------
create or replace function usr.organisation_audit_trg() returns trigger as
$$
begin
	if tg_op = 'INSERT' then
		return new;
	elsif tg_op = 'UPDATE' then
		new.org_change_date := now();
		return new;
	elsif tg_op = 'DELETE' then
		return old;
	end if;

end;
$$ language plpgsql;

drop trigger if exists org_audit on usr.organisation;

create trigger org_audit
BEFORE INSERT OR UPDATE OR delete on usr.organisation
for each row
	execute procedure usr.organisation_audit_trg();


-- -----------------------------------------------------------------------------
-- USER auditing functions/triggers
-- -----------------------------------------------------------------------------
create or replace function usr.user_audit_trg() returns trigger as
$$
begin
	if tg_op = 'INSERT' then
		return new;
	elsif tg_op = 'UPDATE' then
		new.usr_change_date := now();
		return new;
	elsif tg_op = 'DELETE' then
		return old;
	end if;

end;
$$ language plpgsql;

drop trigger if exists usr_audit on usr.user;

create trigger usr_audit
BEFORE INSERT OR UPDATE OR delete on usr.user
for each row
	execute procedure usr.user_audit_trg();


-- -----------------------------------------------------------------------------
-- USER_APPLICATION auditing functions/triggers
-- -----------------------------------------------------------------------------
create or replace function usr.user_application_audit_trg() returns trigger as
$$
begin
	if tg_op = 'INSERT' then
		return new;
	elsif tg_op = 'UPDATE' then
		new.uap_change_date := now();
		return new;
	elsif tg_op = 'DELETE' then
		return old;
	end if;

end;
$$ language plpgsql;

drop trigger if exists uap_audit on usr.user_application;

create trigger uap_audit
BEFORE INSERT OR UPDATE OR delete on usr.user_application
for each row
	execute procedure usr.user_application_audit_trg();


-- -----------------------------------------------------------------------------
-- USER_ORGANISATION auditing functions/triggers
-- -----------------------------------------------------------------------------
create or replace function usr.user_organisation_audit_trg() returns trigger as
$$
begin
	if tg_op = 'INSERT' then
		return new;
	elsif tg_op = 'UPDATE' then
		new.uog_change_date := now();
		return new;
	elsif tg_op = 'DELETE' then
		return old;
	end if;

end;
$$ language plpgsql;

drop trigger if exists uog_audit on usr.user_organisation;

create trigger uog_audit
BEFORE INSERT OR UPDATE OR delete on usr.user_organisation
for each row
	execute procedure usr.user_organisation_audit_trg();


-- -----------------------------------------------------------------------------
-- USER_PROJECT auditing functions/triggers
-- -----------------------------------------------------------------------------
create or replace function usr.user_project_audit_trg() returns trigger as
$$
begin
	if tg_op = 'INSERT' then
		return new;
	elsif tg_op = 'UPDATE' then
		new.upr_change_date := now();
		return new;
	elsif tg_op = 'DELETE' then
		return old;
	end if;

end;
$$ language plpgsql;

drop trigger if exists upr_audit on usr.user_project;

create trigger upr_audit
BEFORE INSERT OR UPDATE OR delete on usr.user_project
for each row
	execute procedure usr.user_project_audit_trg();


