
INSERT INTO usr.organisation (org_name,org_desc,org_create_date,org_create_user,org_change_date,org_change_user) VALUES
	 ('Alle',NULL,'2021-11-17 09:20:21.149467+01','4','2021-11-17 09:20:21.149467+01','apisrv_dba');

INSERT INTO usr.user_organisation (uog_usr_id,uog_org_id,uog_role_code,uog_create_date,uog_create_user,uog_change_date,uog_change_user) VALUES
	 (4,1,'ros-oadm','2021-11-17 09:23:58.361194+01','apisrv_dba','2021-11-17 09:23:58.361194+01','apisrv_dba'),
	 (4,1,'ros-oemp','2021-11-17 09:27:14.022047+01','apisrv_dba','2021-11-17 09:27:14.022047+01','apisrv_dba'),
	 (4,1,'ros-opl','2021-11-17 09:37:18.299356+01','apisrv_dba','2021-11-17 09:37:18.299356+01','apisrv_dba');

--INSERT INTO trf.project (pro_org_id,pro_pro_id,pro_type_code,pro_name,pro_desc,pro_version,pro_create_date,pro_create_user,pro_change_date,pro_change_user) VALUES
--	 (1,NULL,'pro-usr','Vorlage Projekt','Ab diesem Vorlageprojekt werden alle anderen Projekte kopiert.',NULL,'2021-11-17 09:37:53.588515+01','4','2021-11-17 09:45:50.159783+01','4');

--INSERT INTO usr.user_project (upr_usr_id,upr_pro_id,upr_role_code,upr_create_date,upr_create_user,upr_change_date,upr_change_user) VALUES
--	 (4,1,'ros-padm','2021-11-17 09:37:53.588515+01','apisrv_dba','2021-11-17 09:37:53.588515+01','apisrv_dba'),
--	 (4,1,'ros-prd','2021-11-17 09:37:53.588515+01','apisrv_dba','2021-11-17 09:37:53.588515+01','apisrv_dba'),
--	 (4,1,'ros-pwr','2021-11-17 09:37:53.588515+01','apisrv_dba','2021-11-17 09:37:53.588515+01','apisrv_dba');
