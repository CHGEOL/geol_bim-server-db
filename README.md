# GEOL_BIM Server DB

DB backend for GEOL_BIM server

## Prerequisites to make it run
There must be a .creds directory containing a db_password.dev file at least. The root password for the postgres is stored in this file in a single line.
Create an empty C:\_Data\geol_bim-db-postgres directory.

## Storage of the container
All files for the db are stored in /var/lib/postgresql/data and they are lost when the container is rebuilt.

To prevent this use a docker compose file to mount a permanent storage location on the data storage directory.
Like:
```
  db:
    container_name: geol_bim-db
    build:
      context: ../geol_bim-server-db
      dockerfile: Dockerfile.dev
    ports:
      - "127.0.0.1:5438:5432"
    networks:
      - backend
    volumes:
      - C:\_Data\geol_bim-db-postgres:/var/lib/postgresql/data
    restart: always
```

Instead of a volume mount on the local machine also volumes of Docker can be used.

## The setup/ directory

This directory contains all necessary *.sql, *.sql.gz, or *.sh scripts to setup an empty DB for the server (See [postgres container init sql scripts](https://github.com/docker-library/docs/blob/master/postgres/README.md#initialization-scripts)). These files will be executed in sorted name order (as defined by the current locale, which defaults to en_US.utf8) and any *.sql files are executed on the postgres db by the postgres superuser by default.

When setting up the container every \*.sql but those that start with 00*.sql automatically get prefixed by 
```
\connect apisrv
set user apisrv_dba
```
for they are executed on the apisrv db and as apisrv_dba instead of postgres on postgres.



